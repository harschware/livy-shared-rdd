# Description 

Example of working with Livy and getting back results.  Based on the excellent article from [gethue.com][1], but put into ready to execute scripts for easy demonstration purposes.

1. see http://gethue.com/how-to-use-the-livy-spark-rest-job-server-api-for-sharing-spark-rdds-and-contexts/
2. see also: http://gethue.com/how-to-use-the-livy-spark-rest-job-server-for-interactive-spark-2-2/ and http://gethue.com/how-to-use-the-livy-spark-rest-job-server-api-for-submitting-batch-jar-python-and-streaming-spark-jobs/ for other great articles

### Demonstrate querying Livy REST API

Execute scripts in sequential order.   If you see `export <var>=<digit>` in the output then copy paste it before executing the next script.

```
./01startSession.sh 
export SESSION_ID=9

./02postShareable.sh 
export STATEMENT1_ID=0

./03getShareableResult.sh 

./04submitAkPyout.sh 
export STATEMENT2_ID=1

./05getAkPyout.sh 

./06submitAkJsonOut.sh 
export STATEMENT3_ID=2

./07getAkJson.sh 
```

[1]: http://gethue.com/how-to-use-the-livy-spark-rest-job-server-api-for-sharing-spark-rdds-and-contexts/
