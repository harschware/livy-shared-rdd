#!/bin/bash

if [ -z $SESSION_ID ]; then
   >&2 echo "SESSION_ID not defined"
fi

if [ -z $STATEMENT2_ID ]; then
   >&2 echo "STATEMENT2_ID not defined"
fi

curl -s localhost:8998/sessions/${SESSION_ID}/statements/${STATEMENT2_ID} | python -m json.tool

