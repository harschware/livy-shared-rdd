#!/bin/bash

if [ -z $SESSION_ID ]; then
   >&2 echo "SESSION_ID not defined"
fi

if [ -z $STATEMENT3_ID ]; then
   >&2 echo "STATEMENT3_ID not defined"
fi

curl -s localhost:8998/sessions/${SESSION_ID}/statements/${STATEMENT3_ID} | python -m json.tool

