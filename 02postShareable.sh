#!/bin/bash

if [ -z $SESSION_ID ]; then
   >&2 echo "SESSION_ID not defined"
fi

SCALA_CODE=$(cat ShareableRdd.txt | utils/json_escape.sh)
echo $SCALA_CODE

curl -s localhost:8998/sessions/${SESSION_ID}/statements -X POST -H 'Content-Type: application/json' -d "{\"code\":\"${SCALA_CODE}\"}" | python -m json.tool > /tmp/postShareable.$$.json

JSON=$(</tmp/postShareable.$$.json)
echo "$JSON"

ID=$(echo $JSON | jq .id)
cat <<EOF
export STATEMENT1_ID=$ID
EOF

