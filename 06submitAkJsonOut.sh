#!/bin/bash

if [ -z $SESSION_ID ]; then
   >&2 echo "SESSION_ID not defined"
fi

curl -s localhost:8998/sessions/${SESSION_ID}/statements -X POST -H 'Content-Type: application/json' -d '{"code":"data = srdd.get(\"ak\")\n%json data"}' | python -m json.tool > /tmp/submitAkJson.$$.json

JSON=$(</tmp/submitAkJson.$$.json)
echo "$JSON"

ID=$(echo $JSON | jq .id)
cat <<EOF
export STATEMENT3_ID=$ID
EOF

