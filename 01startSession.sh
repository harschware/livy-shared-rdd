#!/bin/bash

curl -s -X POST --data '{"kind": "pyspark"}' -H "Content-Type: application/json" localhost:8998/sessions | python -m json.tool > /tmp/postSessionResult.$$.json

JSON=$(</tmp/postSessionResult.$$.json)

echo "$JSON"

ID=$(echo $JSON | jq .id)
cat <<EOF
export SESSION_ID=$ID
EOF
