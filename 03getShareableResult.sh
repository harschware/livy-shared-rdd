#!/bin/bash

if [ -z $SESSION_ID ]; then
   >&2 echo "SESSION_ID not defined"
fi

if [ -z $STATEMENT1_ID ]; then
   >&2 echo "STATEMENT1_ID not defined"
fi

curl -s localhost:8998/sessions/${SESSION_ID}/statements/${STATEMENT1_ID} | python -m json.tool

