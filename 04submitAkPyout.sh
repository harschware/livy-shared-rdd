#!/bin/bash

if [ -z $SESSION_ID ]; then
   >&2 echo "SESSION_ID not defined"
fi

curl -s localhost:8998/sessions/${SESSION_ID}/statements -X POST -H 'Content-Type: application/json' -d '{"code":"srdd.get(\"ak\")"}' | python -m json.tool > /tmp/submitAkPyout.$$.json

JSON=$(</tmp/submitAkPyout.$$.json)
echo "$JSON"

ID=$(echo $JSON | jq .id)
cat <<EOF
export STATEMENT2_ID=$ID
EOF

